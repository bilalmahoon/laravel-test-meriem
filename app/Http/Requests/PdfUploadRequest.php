<?php

namespace App\Http\Requests;

use App\Services\PdfService;
use Illuminate\Foundation\Http\FormRequest;

class PdfUploadRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        //not searching file and doing dummy test
        return PdfService::searchFor('Proposal');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "file" => "required|mimes:pdf|max:10000"
        ];
    }
}
