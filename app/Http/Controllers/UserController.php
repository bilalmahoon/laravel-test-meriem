<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    protected $country = "Canada";

    //Function that will get us all users who are associated with companies in a given country
    //here inside user->companies than company->pivot->created_at this will be user association time with company
    public function index()
    {
        $country = \request()->get('country', $this->country);
        return User::whereHas('companies', function ($query) use ($country) {
            $query->whereHas('country', function ($query) use ($country) {
                $query->where('countries.name', $country);
            });
        })->with('companies')->get();
    }
}
