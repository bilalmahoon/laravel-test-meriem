<?php

namespace App\Http\Controllers;

use App\Http\Requests\PdfUploadRequest;
use App\Models\Pdf;
use App\Services\PdfService;
use Illuminate\Http\Request;

class PdfController extends Controller
{
    protected $pdfService;
    public function __construct(PdfService $service)
    {
        $this->pdfService = $service;
    }

    /*Before coming in to this function all validations will be done inside request*/
    public function upload(PdfUploadRequest $request)
    {
        return Pdf::updateOrCreate(
            [
                'name' => $request->file('file')->getClientOriginalName(),
                'size' => $request->file('file')->getSize()
            ],
            []
        );
    }
}
