<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $table = 'companies';

    public function users()
    {
        return $this->belongsToMany(User::class, 'company_users', 'company_id', 'user_id')->withTimestamps();
    }

    public function country()
    {
        return $this->belongsTo(Country::class);
    }
}
